import React from 'react';
import { createRoot } from 'react-dom/client';
import drStrange from './strange.jpg';
import './styles.css';

createRoot(document.getElementById('root')).render(
  <div>Hello from React div!</div>
);

export const loadImage = () => {
  const image = new Image();
  image.src = drStrange;
  document.body.appendChild(image);
};
