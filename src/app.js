const button = document.createElement('button');
button.innerHTML = 'Dont Click me!';
button.addEventListener('click', () => {
  import('./imageLoader').then((module) => {
    module.loadImage();
  });
});

document.body.appendChild(button);
